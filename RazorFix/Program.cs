﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RazorFix
{
    // http://stackoverflow.com/questions/12975030/the-custom-tool-razorgenerator-failed-the-method-or-operation-is-not-implemen
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("RazorFix v1.01");
            try
            {
                Console.WriteLine("- Looking for original assemblies...");
                var assemblies = LocateRazorAssemblySources();

                Console.WriteLine("- Looking for a better place to put those assemblies...");
                var destDir = FindDestinationFolder();

                Console.WriteLine("- Reinstaling assemblies...");
                ReinstallAssemblies(assemblies, destDir);

                Console.WriteLine("Done!");
                Console.WriteLine("Please, restart Visual Studio");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static List<string> LocateRazorAssemblySources()
        {
            var appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var vsRootDir = Path.Combine(appDataDir, "Microsoft", "VisualStudio");

            if (!Directory.Exists(vsRootDir))
                throw new Exception("Couldn't find VS's root folder");

            var coreFile = RequireFile("RazorGenerator.Core.dll", vsRootDir);
            var v1File = RequireFile("RazorGenerator.Core.v1.dll", vsRootDir);
            var v2File = RequireFile("RazorGenerator.Core.v2.dll", vsRootDir);

            return new List<string> { coreFile, v1File, v2File };
        }

        private static string FindFile(string fileName, string startFolder)
        {
            return Directory.GetFiles(startFolder, fileName, SearchOption.AllDirectories).OrderBy(n => n).LastOrDefault();
        }

        private static string RequireFile(string fileName, string startFolder)
        {
            var filePath = FindFile(fileName, startFolder);
            if (filePath == null)
                throw new Exception("Couldn't find file '" + fileName + "'");
            return filePath;
        }

        private static string FindDestinationFolder()
        {
            var appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var dl3Dir = Path.Combine(appDataDir, "assembly", "dl3");

            if (!Directory.Exists(dl3Dir))
                throw new Exception("Couldn't find folder for assemblies");

            var existingFile = FindFile("RazorGenerator.Core.dll", dl3Dir);
            if (existingFile == null)
                existingFile = FindFile("RazorGenerator.dll", dl3Dir);

            if (existingFile == null)
                throw new Exception("I guess RazorGenerator has never been installed in here.");

            return Path.GetDirectoryName(existingFile);
        }

        private static void ReinstallAssemblies(List<string> assemblies, string destDir)
        {
            foreach (var assemblyFile in assemblies)
            {
                var destFile = Path.Combine(destDir, Path.GetFileName(assemblyFile));
                File.Copy(assemblyFile, destFile, true);
            }
        }
    }
}
